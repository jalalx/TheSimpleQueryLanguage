## The Simple Query Language

Simple Query Language is an attempt to create a simple query language for REST API end points. Most operators are 
familiar operators based on C like or SQL like languages. This language is not case-sensitive.


Here is a sample query:
```
$(firstName,lastName)$age>=20$firstName(asc)lastName(desc)[10]$
```
Which in SQL is like:
```SQL
SELECT TOP 10 firstName, lastName FROM <data source>
WHERE age >= 20
ORDER BY firstName ASC, lastName desc
```

### Rules

* Statements come in `$<fields>$<predicate>$<order by>[<top>|<paging>]$` format and all of them are optional.
* Predicate expressions come in `<field><operator><field|value>` format.
* OrderBy expressions come in `<field>(asc|desc)` format.
* Top operator can be applied in `[<count>]` format at the end of statement.
* Paging operator can be applied in `[<index>,<size>]` format at the end of statement where `<index>` is zero based and `<size>` is always positive.

### Fields

The first section of query is the fields list. You can set which fields 
need to be returned to the client. Fields list is optional and if you don't declare it, all
fields in the data source will be returned.
```
$(firstName,lastName,dateOfBirth,rate)$
```

### Predicate

The second section of query is the predicate section. You can set conditions for rows to be returned using 
multiple conditions like most cases of what you do in SQL languge. This section is also optional and if you don't declare it
all rows in the data source will be returned. In the Operators section, you can see samples of declaring a predicate.

#### Operators

##### Equal Sign `=`

Use this operator for strings or literals to find exacly matched values:
```
$age=30$
$name='John Smith'$
$birthDate='1990-06-10'$
```
in SQL
```SQL
WHERE age = 30
WHERE name = 'John Smith'
WHERE birthDate = '1990-06-10'
```

Also you can use equal sign to find matches in a range of values:
```
$age=[17,18]$
$color=['red','green','blue']$
```
in SQL
```SQL
WHERE age IN (17,18)
WHERE color IN ('red','green','blue')
```

##### Not Equal Sign `!=`

Use this operator to apply a `NOT` to what ever an equal sign `=` operator does:
```
$age!=18$
$color!=['red','green','blue']$
```
in SQL
```SQL
WHERE age <> 18
WHERE color NOT IN ('red','green','blue')
```

##### Like `%` and Not Like `!%` Signs
Use these signs for matching string literals. For example:
```
$firstName%'%jonh%'$ 
$phoneNumber%'+555%'$ 
$address!%'%mountain%'$ 
$phoneNumber!%'%00'$
```
are like SQL
```SQL
WHERE firstName LIKE '%john%'
WHERE phoneNumber LIKE '+555%'
WHERE address NOT LIKE '%mountain%'
WHERE phoneNumber NOT LIKE '%00'
```

##### Greater than `>`, Greater than or equal `>=`, Less than `<` and Less than or equal `<=` Signs

Use these operators to set boundaries for specific range of data.
```
$age<=17$
$length>10$
```
in SQL
```SQL
WHERE age <= 17
WHERE length > 10
```

##### And `&`, Or `|`, `(` and `)` Signs

Use `&` and `|` operators to join two or more expressions. You can use parantheses to set order for comparing operators.
```
$age>=18&age<=65$
$age>=18&age<=65|gender='male'$
$(age>=18&age<=65|gender='male')&country='UK'$
$(age>=18|(age<=65&gender='male'))&country='UK'$
```
in SQL
```SQL
WHERE age >=18 AND age <= 65
WHERE age >=18 AND age <= 65 OR gender = 'male'
WHERE (age >=18 AND age <= 65 OR gender = 'male') AND country = 'UK'
WHERE (age >=18 OR (age <= 65 OR gender = 'male')) AND country = 'UK'
```

### Sort Expressions

Sort expressions are like SQL language, except you need to set the order type `ASC` or `DESC` explicitly.
For example:
```
$lastName(asc)$
$age(asc)grade(desc)$
```
are equal to SQL:
```SQL
ORDER BY lastName ASC
ORDER BY age ASC, grade DESC
```

##### TOP Operator

To limit number of rows being returned to client use TOP operator like this:
```
$[10]$
```
This will return top 10 rows of the data source. Top operator always appears after sort expressions like:
```
$name(asc)[10]$
```

##### Paging Operator

Use paging opeator to limit number of rows returned from data source. First number specifies page index and
second number specifies max number of rows in that page. For example:
```
$[0,25]$
```
This will return the first 25 rows from the first page of the data source. Paging operator like top operator always 
comes after sort expression:
```
$name(asc)[0,25]$
```

###### NOTE: You can not use Top and Paging operators in the same query.