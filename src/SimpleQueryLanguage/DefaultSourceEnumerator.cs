using System;

namespace SimpleQueryLanguage
{
    public class DefaultSourceEnumerator : ISourceEnumerator
    {
        private int _index = -1;
        private readonly string _source;

        public DefaultSourceEnumerator(string source)
        {
            _source = source ?? throw new System.ArgumentNullException(nameof(source));
        }

        public char Current
        {
            get
            {
                if (_index == -1)
                {
                    throw new InvalidOperationException("MoveNext is not called yet!");
                }

                return _source[_index];
            }
        }

        public int Index => _index;

        public bool MoveNext()
        {
            if (IsCompleted())
            {
                return false;
            }

            _index++;
            return true;
        }

        public bool IsCompleted()
        {
            return _index == _source.Length - 1;
        }

        public char PeekNext()
        {
            if (IsCompleted())
            {
                throw new InvalidOperationException("Source is terminated.");
            }

            return _source[_index + 1];
        }
    }
}
