namespace SimpleQueryLanguage
{
    public interface ISourceEnumerator
    {
        bool IsCompleted();

        bool MoveNext();

        char PeekNext();

        char Current { get; }

        int Index { get; }
    }
}
