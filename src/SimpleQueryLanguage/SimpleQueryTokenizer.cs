using System;
using System.Collections.Generic;

namespace SimpleQueryLanguage
{
    public class SimpleQueryTokenizer
    {
        public IEnumerable<Token> GetTokens(string expression)
        {
            var enumerator = new DefaultSourceEnumerator(expression);
            while (enumerator.MoveNext())
            {
                yield return Parse(enumerator);
            }
        }

        private Token Parse(ISourceEnumerator enumerator)
        {
            switch (enumerator.Current)
            {
                case '$':
                    return new Token { TokenType = TokenType.DollarSign, StartIndex = enumerator.Current, Length = 1 };

                case '(':
                    return new Token { TokenType = TokenType.ParanthesisOpen, StartIndex = enumerator.Current, Length = 1 };

                case ')':
                    return new Token { TokenType = TokenType.ParanthesisClose, StartIndex = enumerator.Current, Length = 1 };

                case '[':
                    return new Token { TokenType = TokenType.BracketOpen, StartIndex = enumerator.Current, Length = 1 };

                case ']':
                    return new Token { TokenType = TokenType.BracketClose, StartIndex = enumerator.Current, Length = 1 };

                case '=':
                    return new Token { TokenType = TokenType.EqualSign, StartIndex = enumerator.Current, Length = 1 };

                case '!':
                    return GetNegativeToken(enumerator);

                case '%':
                    return new Token { TokenType = TokenType.LikeSign, StartIndex = enumerator.Current, Length = 1 };

                case '>':
                    return new Token { TokenType = TokenType.BracketClose, StartIndex = enumerator.Current, Length = 1 };

                case '<':
                    return new Token { TokenType = TokenType.BracketClose, StartIndex = enumerator.Current, Length = 1 };

                case '\'':
                    return GetStringOrDateLiteral(enumerator);

                default:
                    if (char.IsLetter(enumerator.Current))
                    {
                        return GetIdentifierToken(enumerator);
                    }
                    else
                    {
                        throw new SyntaxErrorException(enumerator.Index);
                    }
            }
        }


        private Token GetIdentifierToken(ISourceEnumerator enumerator)
        {
            var startIndex = enumerator.Index;
            var identifier = enumerator.Current.ToString();
            while (enumerator.MoveNext())
            {
                if (char.IsLetterOrDigit(enumerator.Current))
                {
                    identifier += enumerator.Current;
                }
                else
                {
                    break;
                }
            }

            return new Token { TokenType = TokenType.Identifier, StartIndex = startIndex, Length = enumerator.Index - startIndex };
        }

        private Token GetNegativeToken(ISourceEnumerator enumerator)
        {
            var previousChar = enumerator.Current;
            if (enumerator.MoveNext())
            {
                current = enumerator.Current;
                switch (current)
                {
                    case '=':
                        return new Token { StartIndex = enumerator.Index - 1, Length = 2, TokenType = TokenType.NotEqualSign };

                    case '%':
                        return new Token { StartIndex = enumerator.Index - 1, Length = 2, TokenType = TokenType.NotLikeSign };
                }
            }

            throw new SyntaxErrorException(enumerator.Index - 1);
        }

        private Token GetStringOrDateLiteral(ISourceEnumerator enumerator)
        {
            var stringOrDateLiteral = new string(new[] { enumerator.Current });
            var previousChar = enumerator.Current;
            var innerIndex = enumerator.Index;

            while (enumerator.MoveNext())
            {
                innerIndex++;
                if (enumerator.Current == '\\')
                {
                    previousChar = enumerator.Current;
                    continue;
                }
                else if (enumerator.Current == '\'' && previousChar != '\\')
                {
                    break;
                }

                stringOrDateLiteral += enumerator.Current;
                previousChar = enumerator.Current;
            }

            if (DateTime.TryParse(stringOrDateLiteral, out DateTime dateTime))
            {
                return new Token { StartIndex = enumerator.Index, Length = innerIndex - enumerator.Index, TokenType = TokenType.LiteralDateTime };
            }
            else
            {
                return new Token { StartIndex = enumerator.Index, Length = innerIndex - enumerator.Index, TokenType = TokenType.LiteralString };
            }
        }
    }
}
