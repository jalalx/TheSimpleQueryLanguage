using System;

namespace SimpleQueryLanguage
{
    public class SyntaxErrorException : Exception
    {
        public SyntaxErrorException(int index) : base($"Syntex error at index {index}.")
        {
            Index = index;
        }

        public int Index { get; }
    }
}
