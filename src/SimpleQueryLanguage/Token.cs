namespace SimpleQueryLanguage
{
    public class Token
    {
        public TokenType TokenType { get; set; }

        public int StartIndex { get; set; }

        public int Length { get; set; }
    }
}
