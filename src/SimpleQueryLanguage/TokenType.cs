namespace SimpleQueryLanguage
{
    public enum TokenType
    {
        DollarSign,
        EqualSign,
        NotEqualSign,
        LikeSign,
        NotLikeSign,
        GreaterThanSign,
        GreaterThanOrEqualSign,
        LessThanSign,
        LessThanOrEqualSign,
        Identifier,
        ParanthesisOpen,
        ParanthesisClose,
        BracketOpen,
        BracketClose,
        LiteralString,
        LiteralNumeric,
        LiteralTime,
        LiteralDateTime,
        LiteralBoolean,
    }
}
