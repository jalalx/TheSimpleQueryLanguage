using System;
using System.Linq;
using Xunit;

namespace SimpleQueryLanguage.Tests
{
    public class DefaultSourceEnumeratorTests
    {
        [Fact]
        public void IsCompleted_ForEmptySource_ReturnsTrue()
        {
            var source = "";
            var enumerator = new DefaultSourceEnumerator(source);

            var actual = enumerator.IsCompleted();
            Assert.True(actual);
        }

        [Fact]
        public void IsCompleted_ForNonEmptySource_ReturnsFalse()
        {
            var source = "$";
            var enumerator = new DefaultSourceEnumerator(source);

            var actual = enumerator.IsCompleted();
            Assert.False(actual);
        }

        [Theory]
        [InlineData("$age=10$")]
        [InlineData("$(firstName,lastName)$age=10$")]
        [InlineData("$(firstName,lastName)$age=10$age(asc)$")]
        [InlineData("$(firstName,lastName)$age=10$[10]$")]
        [InlineData("$(firstName,lastName)$age=10$age(asc)[1,10]$")]
        public void IsCompleted_WithCallingMoveNextToTheEndOfSource_ReturnsTrue(string source)
        {
            var enumerator = new DefaultSourceEnumerator(source);
            while (enumerator.MoveNext()) ;

            var actual = enumerator.IsCompleted();
            Assert.True(actual);
        }

        [Fact]
        public void Current_WithoutCallingMoveNext_ThrowsInvalidOperationException()
        {
            var source = "$age=18$";
            var enumerator = new DefaultSourceEnumerator(source);

            var ex = Record.Exception(() => enumerator.Current);
            Assert.IsType<InvalidOperationException>(ex);
        }

        [Theory]
        [InlineData("$age=18$", 1, '$')]
        [InlineData("$age=18$", 5, '=')]
        [InlineData("$age=18$", 8, '$')]
        public void Current_WithCallingMoveNext_ReturnsExpectedChar(string source, int position, char expected)
        {
            var enumerator = new DefaultSourceEnumerator(source);
            while (position > 0)
            {
                enumerator.MoveNext();
                position--;
            }

            var actual = enumerator.Current;

            Assert.Equal(expected, actual);
        }
    }
}