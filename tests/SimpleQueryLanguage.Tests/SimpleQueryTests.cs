using System;
using Xunit;

namespace SimpleQueryLanguage.Tests
{
    public class SimpleQueryTests
    {
        [Fact]
        public void Parse_EmptyQuery_ReturnsValidQUery()
        {
            var query = string.Empty;

            var expectedResult = SimpleQuery.TryParse(query, out SimpleQuery model);

            Assert.True(expectedResult);
        }
    }
}
